<?php

namespace Watson\Document\Providers;

use App\User;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Watson\Document\Category;
use Watson\Document\Contracts\CategoryProvider;
use Watson\Document\Contracts\CategoryRepositoryInterface;
use Watson\Document\Contracts\DocumentComposerInterface;
use Watson\Document\Contracts\DocumentFormComposerInterface;
use Watson\Document\Contracts\DocumentListComposerInterface;
use Watson\Document\Contracts\DocumentPolicyInterface;
use Watson\Document\Contracts\DocumentProvider;
use Watson\Document\Contracts\DocumentListPresenterInterface;
use Watson\Document\Contracts\DocumentRepositoryInterface;
use Watson\Document\Contracts\DocumentUserProviderInterface;
use Watson\Document\Contracts\MarkdownParserInterface;
use Watson\Document\Document;
use Watson\Document\Http\View\Composers\DocumentComposer;
use Watson\Document\Http\View\Composers\DocumentFormComposer;
use Watson\Document\Http\View\Composers\DocumentListComposer;
use Watson\Document\MarkdownResolver;
use Watson\Document\Policies\DocumentPolicy;
use Watson\Document\Presenters\DocumentListPresenter;
use Watson\Document\Presenters\DocumentPresenter;
use Watson\Document\Repositories\CategoryRepository;
use Watson\Document\Repositories\DocumentRepository;
use Watson\Document\Contracts\DocumentPresenterInterface;

/**
 * Class DocumentServiceProvider
 * @package Watson\Document\Providers
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
class DocumentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
        $this->loadViewsFrom(__DIR__ . '/../../views', 'document');
        $this->registerViews();
        $this->registerDirectives();
        $this->registerRoutes();
        $this->registerAbilities();
        $this->registerPolicies();

        $this->publishes(
            [
                __DIR__ . '/../../config/document.php' => config_path('document.php'),
            ]
        );

        $this->app->bind(
            DocumentPolicyInterface::class,
            DocumentPolicy::class
        );
        $this->app->bind(
            DocumentListComposerInterface::class,
            DocumentListComposer::class
        );
        $this->app->bind(
            DocumentComposerInterface::class,
            DocumentComposer::class
        );
        $this->app->bind(
            DocumentPresenterInterface::class,
            DocumentPresenter::class
        );
        $this->app->bind(
            DocumentListPresenterInterface::class,
            DocumentListPresenter::class
        );
        $this->app->bind(
            DocumentRepositoryInterface::class,
            DocumentRepository::class
        );
        $this->app->bind(
            DocumentProvider::class,
            Document::class
        );
        $this->app->bind(
            CategoryRepositoryInterface::class,
            CategoryRepository::class
        );
        $this->app->bind(
            CategoryProvider::class,
            Category::class
        );
        $this->app->bind(
            MarkdownParserInterface::class,
            MarkdownResolver::class
        );
        $this->app->bind(
            DocumentFormComposerInterface::class,
            DocumentFormComposer::class
        );
        $this->app->bind(
            DocumentUserProviderInterface::class,
            User::class
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/document.php', 'document');
    }

    /**
     * Registers custom directives and views
     */
    public function registerViews()
    {
        view()->composer('document::documents.partials.list', DocumentListComposerInterface::class);
        view()->composer('document::documents.partials.document', DocumentComposerInterface::class);
        view()->composer('document::documents.partials.form', DocumentFormComposerInterface::class);
        view()->composer('document::documents.partials.new', DocumentFormComposerInterface::class);
    }

    /**
     * Registers custom Blade directive
     */
    public function registerDirectives()
    {
        Blade::directive(
            'documents',
            function ($expression) {
                switch ($expression) {
                    case 'form':
                        return '<?php echo view("document::documents.partials.form")->render() ?>;';
                        break;
                    case 'document':
                    case 'single':
                        return '<?php echo view("document.documents.partials.document")->render() ?>';
                        break;
                    case 'list':
                    default:
                        return '<?php echo view("document::documents.partials.list")->render() ?>';
                }
            }
        );
    }

    /**
     * Registers routes
     */
    public function registerRoutes()
    {
        $documents = Route::resource('documents', 'Watson\\Document\\Http\\Controllers\\DocumentController')
                          ->middleware(
                              config('document.middleware')
                          );
        $categories = Route::resource('categories', 'Watson\\Document\\Http\\Controllers\\CategoryController')
                           ->middleware(
                               config('document.middleware')
                           );
    }

    /**
     * Registers auth policies
     */
    public function registerPolicies()
    {
        Gate::policy(DocumentProvider::class, DocumentPolicyInterface::class);
    }

    /**
     * Registers abilities
     */
    public function registerAbilities()
    {
        Gate::resource('documents', DocumentPolicyInterface::class);
        Gate::define('documents.index', 'Watson\\Document\\Contracts\\DocumentPolicyInterface@index');
    }
}
