<?php

namespace Watson\Document\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Watson\Document\Contracts\DocumentProvider;
use Watson\Document\Http\Requests\StoreDocumentRequest;
use Watson\Document\Contracts\DocumentRepositoryInterface;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class DocumentController extends Controller
{
    use AuthorizesRequests;

    /**
     * @var DocumentRepositoryInterface
     */
    protected $documentRepository;

    /**
     * @var DocumentProvider
     */
    protected $document;

    /**
     * DocumentController constructor.
     * @param DocumentRepositoryInterface $documentRepository
     */
    public function __construct(
        DocumentRepositoryInterface $documentRepository
    )
    {
        $this->documentRepository = $documentRepository;
        $this->authorizeResource(DocumentProvider::class, 'document');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('documents.index');
        return view('document::documents.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('document::documents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreDocumentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreDocumentRequest $request)
    {
        try {
            $model = $this->documentRepository->create(
                $this->documentRepository->prepare($request->toArray())->setAttribute('user_id', auth()->id())
            );

            return redirect()->route('document.edit', ['slug' => $model->getSlug()]);
        } catch (\Throwable $exception) {
            app('log')->error($exception->getMessage(), $exception->getTrace());
            $request->session()->flash('error', 'Error saving model');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  DocumentProvider|Model $document
     * @return \Illuminate\Http\Response
     */
    public function show(DocumentProvider $document)
    {
        return view('document::documents.show', compact('document'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  DocumentProvider $document
     * @return \Illuminate\Http\Response
     */
    public function edit(DocumentProvider $document)
    {
        return view('document::documents.edit', compact('document'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            if ($document = $request->route('document')) {
                $document = $this->documentRepository->retrieveBy('slug', $document);
                $this->documentRepository->update($document->getId(), $request->only(['title', 'content']));
                $document->setAttribute('user_id', auth()->id())->saveOrFail();
                $request->session()->flash('success', 'Document saved successfully');
                return redirect()->route('documents.edit', ['slug' => $document->getSlug()]);
            }
        } catch (\Throwable $exception) {
            $request->session()->flash('error', 'Error saving document');
            redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DocumentProvider $document)
    {
        try {
            $this->documentRepository->remove($document->getId());
        } catch (\Exception $exception) {
            app('log')->error('No such entity found.');
        }

        session()->flash('success', 'Removed document');
        return redirect()->route('document.index');
    }
}
