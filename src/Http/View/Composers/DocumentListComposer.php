<?php

namespace Watson\Document\Http\View\Composers;

use Watson\Document\Contracts\DocumentComposerInterface;
use Illuminate\Contracts\View\View;
use Watson\Document\Contracts\DocumentPresenterInterface;
use Watson\Document\Contracts\DocumentRepositoryInterface;
use Watson\Document\Contracts\DocumentListPresenterInterface;

/**
 * Class DocumentListComposer
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
class DocumentListComposer implements DocumentComposerInterface
{
    /**
     * @var DocumentRepositoryInterface
     */
    protected $documentRepository;

    /**
     * @var DocumentPresenterInterface
     */
    protected $documentPresenter;

    /**
     * DocumentListComposer constructor.
     * @param DocumentRepositoryInterface $documentRepository
     */
    public function __construct(
        DocumentRepositoryInterface $documentRepository
    )
    {
        $this->documentRepository = $documentRepository;
    }

    /**
     * @param View $view
     * @return mixed|void
     */
    public function compose(View $view)
    {
        $documents = $this->documentRepository->all();
        $block = app()->makeWith(
            DocumentListPresenterInterface::class,
            compact('documents')
        );
        $view->with(compact('block'));
    }
}