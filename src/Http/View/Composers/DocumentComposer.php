<?php

namespace Watson\Document\Http\View\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Watson\Document\Contracts\DocumentPresenterInterface;
use Watson\Document\Contracts\DocumentProvider;
use Watson\Document\Contracts\DocumentRepositoryInterface;

/**
 * Class DocumentComposer
 * @package Watson\Document\Http\View\Composers
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
class DocumentComposer
{

    /**
     * @var DocumentRepositoryInterface
     */
    protected $documentRepository;

    /**
     * @var DocumentProvider
     */
    protected $document;

    /**
     * DocumentComposer constructor.
     * @param DocumentRepositoryInterface $documentRepository
     */
    public function __construct(
        DocumentRepositoryInterface $documentRepository
    )
    {
        $this->documentRepository = $documentRepository;
    }

    /**
     * @param View $view
     * @return View
     */
    public function compose(View $view)
    {
        /** @var array $data */
        $data = $view->getData();
        if (array_key_exists('document', $data) && $data['document'] instanceof DocumentProvider) {
            $document = $data['document'];
            $block = app()->makeWith(DocumentPresenterInterface::class, compact('document'));
            return $view->with(compact('block'));
        }
    }
}