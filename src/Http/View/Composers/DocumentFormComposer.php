<?php

namespace Watson\Document\Http\View\Composers;

use Watson\Document\Contracts\DocumentFormComposerInterface;
use Illuminate\Contracts\View\View;
use Watson\Document\Contracts\DocumentPresenterInterface;
use Watson\Document\Contracts\DocumentProvider;

class DocumentFormComposer extends DocumentComposer implements DocumentFormComposerInterface
{
    /**
     * @var DocumentProvider $document
     */
    protected $document;

    /**
     * @param View $view
     * @return View|mixed
     */
    public function compose(View $view)
    {
        /** @var array $data */
        $data = $view->getData();
        if (array_key_exists('document', $data) && $data['document'] instanceof DocumentProvider) {
            $document = $data['document'];
            $block = app()->makeWith(DocumentPresenterInterface::class, compact('document'));
            return $view->with(compact('block'));
        }
        $document = app()->make(DocumentProvider::class);
        $block = app()->makeWith(DocumentPresenterInterface::class, compact('document'));
        return $view->with(compact('block'));
    }
}