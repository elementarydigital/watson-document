<?php

namespace Watson\Document\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreDocumentRequest
 * @package Watson\Document\Http\Requests
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
class StoreDocumentRequest extends FormRequest
{
    protected function getValidatorInstance()
    {
        $this->merge(
            [
                'slug' => str_slug($this->get('slug'))
            ]
        );
        return parent::getValidatorInstance();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !auth()->guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug'    => 'required|unique:documents',
            'title'   => 'required',
            'content' => 'required'
        ];
    }
}
