<?php

namespace Watson\Document\Presenters;

use Illuminate\Support\Collection;
use Watson\Document\Contracts\DocumentListPresenterInterface;

/**
 * Class DocumentListPresenter
 * @package Watson\Document\Providers
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
class DocumentListPresenter implements DocumentListPresenterInterface
{
    /**
     * @var Collection
     */
    protected $documents;

    /**
     * DocumentListPresenter constructor.
     * @param Collection $documents
     */
    public function __construct(
        Collection $documents
    )
    {
        $this->documents = $documents;
    }

    /**
     * @return Collection
     */
    public function documents()
    {
        return $this->documents;
    }
}