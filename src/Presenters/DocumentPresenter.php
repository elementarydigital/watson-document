<?php

namespace Watson\Document\Presenters;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Watson\Document\Contracts\DocumentProvider;
use Watson\Document\Contracts\DocumentPresenterInterface;

/**
 * Class DocumentPresenter
 * @package Watson\Document\Presenters
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
class DocumentPresenter implements DocumentPresenterInterface
{
    /**
     * @var DocumentProvider
     */
    protected $document;

    /**
     * DocumentPresenter constructor.
     * @param DocumentProvider $document
     */
    public function __construct(
        DocumentProvider $document
    )
    {
        $this->document = $document;
    }

    /**
     * @return DocumentProvider|Model
     */
    public function document()
    {
        return $this->document;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return Carbon::createFromTimestamp(strtotime($this->document->getUpdatedAt()))->diffForHumans();
    }

    /**
     * @param string $key
     * @return string
     */
    public function getUserName($key = 'name')
    {
        return $this->document->getUser()->getAttribute($key);
    }
}