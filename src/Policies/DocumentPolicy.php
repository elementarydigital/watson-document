<?php

namespace Watson\Document\Policies;

use App\User;
use Watson\Document\Contracts\DocumentPolicyInterface;
use Watson\Document\Contracts\DocumentProvider;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class DocumentPolicy
 * @package Watson\Document\Policies
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
class DocumentPolicy implements DocumentPolicyInterface
{
    use HandlesAuthorization;

    /**
     * Determines whether the user can list documents.
     *
     * @param User $user
     * @return bool
     */
    public function index(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the document.
     *
     * @param  \App\User  $user
     * @param  DocumentProvider  $document
     * @return mixed
     */
    public function view(User $user, DocumentProvider $document)
    {
        return true;
    }

    /**
     * Determine whether the user can create documents.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the document.
     *
     * @param  \App\User  $user
     * @param  DocumentProvider $document
     * @return mixed
     */
    public function update(User $user, DocumentProvider $document)
    {
        return true;
    }

    /**
     * Determine whether the user can delete the document.
     *
     * @param  \App\User  $user
     * @param  DocumentProvider  $document
     * @return mixed
     */
    public function delete(User $user, DocumentProvider $document)
    {
        return true;
    }
}
