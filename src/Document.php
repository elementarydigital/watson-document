<?php

namespace Watson\Document;

use App\User;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Database\Eloquent\Model;
use Watson\Document\Concerns\CanParseMarkdown;
use Watson\Document\Contracts\CategoryProvider;
use Watson\Document\Contracts\DocumentProvider;
use Watson\Document\Contracts\DocumentUserProviderInterface;

/**
 * Class Document
 * @package Watson\Document
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
class Document extends Model implements DocumentProvider
{
    use CanParseMarkdown;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var array
     */
    protected $fillable = ['slug', 'title', 'content'];

    /**
     * @return mixed
     */
    public function getId()
    {
        $this->getAttributes();
        return $this->getAttribute('id');
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getAttribute('title');
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->getAttribute('slug');
    }

    /**
     * @return Model
     */
    public function getUser()
    {
        if (!$this->user) {
            $this->user = $this->belongsTo(
                $this->getUserInstance(),
                'user_id',
                'id',
                'users'
            )->first(['*']);
        }

        return $this->user;
    }

    /**
     * @return mixed
     */
    protected function getUserInstance()
    {
        return app()->make(DocumentUserProviderInterface::class);
    }

    /**
     * @param bool $format
     * @return mixed
     */
    public function getContent($format = true)
    {
        if ($format) {
            return $this->getMarkdownParser()->convertToHtml($this->getAttribute('content'));
        }

        return $this->getAttribute('content');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getCategories()
    {
        return $this->hasMany(
            app()->make(CategoryProvider::class)
        )->getResults();
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->getAttribute($this->getCreatedAtColumn());
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->getAttribute($this->getUpdatedAtColumn());
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
