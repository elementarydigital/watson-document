<?php

namespace Watson\Document;

use Watson\Document\Contracts\MarkdownParserInterface;

/**
 * Class MarkdownResolver
 * @package Watson\Document
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
class MarkdownResolver implements MarkdownParserInterface
{
    /**
     * @return object
     */
    public function getInstance()
    {
        return app()->make('markdown');
    }

    /**
     * @param $content
     * @return string
     */
    public function convertToHtml($content)
    {
       return $this->getInstance()->convertToHtml($content);
    }
}