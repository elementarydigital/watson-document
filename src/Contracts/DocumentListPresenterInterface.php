<?php

namespace Watson\Document\Contracts;

use Illuminate\Support\Collection;

/**
 * Interface DocumentListPresenterInterface
 * @package Watson\Document\Contracts
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
interface DocumentListPresenterInterface
{
    /**
     * @return Collection
     */
    public function documents();
}