<?php

namespace Watson\Document\Contracts;

/**
 * Interface DocumentFormComposerInterface
 * @package Watson\Document\Contracts
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
interface DocumentFormComposerInterface extends DocumentComposerInterface
{

}