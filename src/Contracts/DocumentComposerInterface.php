<?php

namespace Watson\Document\Contracts;

use Illuminate\Contracts\View\View;

/**
 * Interface DocumentComposerInterface
 * @package Watson\Document\Contracts
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
interface DocumentComposerInterface
{
    /**
     * @param View $view
     * @return mixed
     */
    public function compose(View $view);
}