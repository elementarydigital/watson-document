<?php

namespace Watson\Document\Contracts;

/**
 * Interface DocumentListComposerInterface
 * @package Watson\Document\Contracts
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
interface DocumentListComposerInterface extends DocumentComposerInterface
{

}