<?php

namespace Watson\Document\Contracts;

use App\User;
use Illuminate\Contracts\Routing\UrlRoutable;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface DocumentInterface
 * @package Watson\Document\Contracts
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
interface DocumentProvider extends UrlRoutable
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param $format
     * @return string
     */
    public function getContent($format = true);

    /**
     * @return string
     */
    public function getSlug();

    /**
     * @return array
     */
    public function getCategories();

    /**
     * @return Model|User
     */
    public function getUser();

    /**
     * @return string
     */
    public function getUpdatedAt();

    /**
     * @return string
     */
    public function getCreatedAt();
}