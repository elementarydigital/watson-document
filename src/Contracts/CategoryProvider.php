<?php

namespace Watson\Document\Contracts;

/**
 * Interface CategoryProvider
 * @package Watson\Document\Contracts
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
interface CategoryProvider
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getName();

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getDocuments();

    /**
     * @return mixed
     */
    public function getCount();

    /**
     * @return mixed
     */
    public function getCreatedAt();

    /**
     * @return mixed
     */
    public function getUpdatedAt();

    /**
     * @param array $ids
     * @return mixed
     */
    public function attachDocument(array $ids);

    /**
     * @param array $ids
     * @return mixed
     */
    public function detachDocument(array $ids);
}