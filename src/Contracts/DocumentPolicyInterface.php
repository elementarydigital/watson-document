<?php

namespace Watson\Document\Contracts;

/**
 * Interface DocumentPolicyInterface
 * @package Watson\Document\Contracts
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
interface DocumentPolicyInterface
{

}