<?php

namespace Watson\Document\Contracts;

/**
 * Interface MardownParserInterface
 * @package Watson\Document\Contracts
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
interface MarkdownParserInterface
{
    /**
     * @return object
     */
    public function getInstance();

    /**
     * @param $content
     * @return string
     */
    public function convertToHtml($content);
}