<?php

namespace Watson\Document\Contracts;

use Illuminate\Support\Collection;

/**
 * Interface DocumentRepositoryInterface
 * @package Watson\Document\Contracts
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
interface DocumentRepositoryInterface
{
    /**
     * @param array $columns
     * @return Collection
     */
    public function all($columns = ['*']);

    /**
     * @param int $perPage
     * @param array $columns
     * @return Collection
     */
    public function paginate($perPage = 15, $columns = ['*']);

    /**
     * @param $field
     * @param $value
     * @param array $columns
     * @return DocumentProvider|\Illuminate\Database\Eloquent\Model
     */
    public function retrieveBy($field, $value, $columns = ['*']);

    /**
     * @param $id
     * @param array $columns
     * @return DocumentProvider|\Illuminate\Database\Eloquent\Model
     */
    public function retrieveById($id, $columns = ['*']);

    /**
     * @param DocumentProvider|\Illuminate\Database\Eloquent\Model $document
     * @return DocumentProvider|\Illuminate\Database\Eloquent\Model
     * @throws \Throwable
     */
    public function create(DocumentProvider $document);

    /**
     * @param array $data
     * @return DocumentProvider|\Illuminate\Database\Eloquent\Model
     */
    public function prepare(array $data);

    /**
     * @param $id
     * @return mixed
     */
    public function remove($id);

    /**
     * @param $id
     * @param array $data
     * @return DocumentProvider|\Illuminate\Database\Eloquent\Model
     */
    public function update($id, array $data);
}