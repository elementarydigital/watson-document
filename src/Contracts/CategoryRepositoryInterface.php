<?php

namespace Watson\Document\Contracts;

use Illuminate\Support\Collection;

/**
 * Interface CategoryRepositoryInterface
 * @package Watson\Document\Contracts
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
interface CategoryRepositoryInterface
{
    /**
     * @param array $columns
     * @return Collection
     */
    public function all($columns = ['*']);

    /**
     * @param int $perPage
     * @param array $columns
     * @return Collection
     */
    public function paginate($perPage = 15, $columns = ['*']);

    /**
     * @param $field
     * @param $value
     * @param array $columns
     * @return CategoryProvider
     */
    public function retrieveBy($field, $value, $columns = ['*']);

    /**
     * @param $id
     * @param array $columns
     * @return CategoryProvider
     */
    public function retrieveById($id, $columns = ['*']);

    /**
     * @param array $data
     * @return CategoryProvider
     */
    public function create(array $data);

    /**
     * @param $id
     * @return bool|null
     */
    public function remove($id);

    /**
     * @param $id
     * @param array $data
     * @return CategoryProvider
     */
    public function update($id, array $data);
}