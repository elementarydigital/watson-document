<?php

namespace Watson\Document\Contracts;

/**
 * Interface DocumentPresenterInterface
 * @package Watson\Document\Contracts
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
interface DocumentPresenterInterface
{
    /**
     * @return DocumentProvider
     */
    public function document();
}