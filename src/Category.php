<?php

namespace Watson\Document;

use Illuminate\Database\Eloquent\Model;
use Watson\Document\Contracts\CategoryProvider;
use Watson\Document\Contracts\DocumentProvider;

/**
 * Class Category
 * @package Watson\Document
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
class Category extends Model implements CategoryProvider
{
    /**
     * @var array
     */
    protected $fillable = ['id', 'name'];

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->getAttribute('id');
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->getAttribute('name');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getDocuments()
    {
        return $this->belongsToMany(
            app()->make(DocumentProvider::class)
        )->getResults();
    }

    /**
     * @param $ids
     * @return $this
     */
    public function attachDocument(array $ids)
    {
        $this->belongsToMany(
            app()->make(DocumentProvider::class)
        )->attach($ids);

        return $this;
    }

    /**
     * @param array $ids
     * @return $this
     */
    public function detachDocument(array $ids)
    {
        $this->belongsToMany(
            app()->make
        )->detach($ids);

        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->hasMany(
            app()->make(DocumentProvider::class)
        )->count();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->getAttribute($this->getUpdatedAtColumn());
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->getAttribute($this->getCreatedAtColumn());
    }
}
