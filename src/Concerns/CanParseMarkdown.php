<?php

namespace Watson\Document\Concerns;

use Watson\Document\Contracts\MarkdownParserInterface;

trait CanParseMarkdown
{
    /**
     * @return object
     */
    public function getMarkdownParser()
    {
        return app()->make(MarkdownParserInterface::class);
    }
}