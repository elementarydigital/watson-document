<?php

namespace Watson\Document\Repositories;

use Illuminate\Support\Collection;
use Watson\Document\Contracts\DocumentProvider;
use Watson\Document\Contracts\DocumentRepositoryInterface;
use Watson\Document\Document;

/**
 * Class DocumentRepository
 * @package Watson\Document\Repositories
 * @author Liam Firth <liam@elementarydigital.co.uk>
 * @copyright Elementary Digital 2019
 */
class DocumentRepository implements DocumentRepositoryInterface
{
    /**
     * @var Document
     */
    protected $document;

    /**
     * DocumentRepository constructor.
     * @param Document $document
     */
    public function __construct(
        Document $document
    )
    {
        $this->document = $document;
    }

    /**
     * @param array $columns
     * @return \Illuminate\Support\Collection
     */
    public function all($columns = ['*'])
    {
        return $this->document->all($columns);
    }

    /**
     * @param int $perPage
     * @param array $columns
     * @return Collection
     */
    public function paginate($perPage = 15, $columns = ['*'])
    {
        $items = $this->document->newQuery()->paginate($perPage, $columns)->items() ?: [];
        return Collection::make($items);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model|DocumentProvider $document
     * @return \Illuminate\Database\Eloquent\Model|DocumentProvider
     * @throws \Throwable
     */
    public function create(DocumentProvider $document)
    {
        return $document->saveOrFail();
    }

    /**
     * @param $id
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection
     */
    public function update($id, array $data)
    {
        $model = $this->document->newQuery()->findOrFail($id);
        $model->update($data);
        return $model;
    }

    /**
     * @param $id
     * @return bool|null
     */
    public function remove($id)
    {
        try {
            $model = $this->document->newQuery()->findOrFail($id);
            return $model->delete();
        } catch (\Exception $exception) {
            app('log')->error($exception->getMessage());
        }
    }

    /**
     * @param $id
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Model|DocumentProvider
     */
    public function retrieveById($id, $columns = ['*'])
    {
        return $this->document->newQuery()->findOrFail($id, $columns)->first();
    }

    /**
     * @param $field
     * @param $value
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Model|DocumentProvider
     */
    public function retrieveBy($field, $value, $columns = ['*'])
    {
        return $this->document->newQuery()->where($field, '=', $value)->firstOrFail($columns);
    }

    /**
     * Prepares a new model with mass assignable attributes
     *
     * @param array $data
     * @return mixed
     */
    public function prepare(array $data)
    {
        return $this->document->newInstance($data);
    }
}