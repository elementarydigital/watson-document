<?php

namespace Watson\Document\Repositories;

use Watson\Document\Category;
use Watson\Document\Contracts\CategoryRepositoryInterface;

class CategoryRepository implements CategoryRepositoryInterface
{
    /**
     * @var Category
     */
    protected $category;

    /**
     * CategoryRepository constructor.
     * @param Category $category
     */
    public function __construct(
        Category $category
    )
    {
        $this->category = $category;
    }

    /**
     * @param array $columns
     * @return  \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all($columns = ['*'])
    {
        return $this->category->all($columns);
    }

    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|\Watson\Document\Contracts\CategoryProvider
     */
    public function create(array $data)
    {
        return $this->category->newQuery()->create($data);
    }

    /**
     * @param int $perPage
     * @param array $columns
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function paginate($perPage = 15, $columns = ['*'])
    {
        return $this->category->newQuery()->paginate($perPage, $columns);
    }

    /**
     * @param $id
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Watson\Document\Contracts\CategoryProvider|bool
     */
    public function update($id, array $data)
    {
        try {
            $category = $this->category->newQuery()->findOrFail($id);
            $category->update($data);
            return $category;
        } catch (\Exception $exception) {
            app('log')->error($exception->getMessage());
        }

        return false;
    }

    /**
     * @param $field
     * @param $value
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function retrieveBy($field, $value, $columns = ['*'])
    {
        return $this->category->newQuery()->where($field, '=', $value)->get($columns);
    }

    /**
     * @param $id
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|bool
     */
    public function retrieveById($id, $columns = ['*'])
    {
        try {
            return $this->category->newQuery()->findOrFail($id, $columns);
        } catch (\Exception $exception) {
            app('log')->error($exception->getMessage());
        }

        return false;
    }

    /**
     * @param $id
     * @return bool|null
     */
    public function remove($id)
    {
        try {
            $category = $this->category->newQuery()->findOrFail($id);
            return $category->delete();
        } catch (\Exception $exception) {
            app('log')->error($exception->getMessage());
        }

        return false;
    }
}