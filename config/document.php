<?php

return [
    'prefix' => 'document',
    'middleware' => [
        'web',
        'auth'
    ]
];