<?php /** @var \Watson\Document\Presenters\DocumentPresenter $block */ ?>
<form action="{{ $block->getFormAction() }}" method="POST">
    <input type="hidden" name="_method" value="{{ $block->getFormMethod() }}" />
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <input type="hidden" name="id" value="{{ $block->document()->getId() }}" />
    <dl>
        <dt>{{ __('Title') }}</dt>
        <dd><input name="title" type="text" value="{{ $block->document()->getTitle() }}" /></dd>
        <dt>{{ __('Slug') }}</dt>
        <dd><input name="slug" type="text" disabled="disabled" value="{{ $block->document()->getSlug() }}" /></dd>
        <dt>{{ __('Content') }}</dt>
        <dd><textarea name="content">{{ $block->document()->getContent(false) }}</textarea></dd>
    </dl>
    <a href="{{ route('documents.index') }}">{{ __('Back') }}</a>
    <input type="submit" name="submit" value="{{ __('Save') }}" />
</form>