<?php /** @var \Watson\Document\Presenters\DocumentListPresenter $block */ ?>
# Documents: {{ $block->documents()->count() }}
<ul>
    @foreach($block->documents() as $document)
        <li><a href="{{ route('documents.show', compact('document')) }}">{{ $document->getTitle() }}</a></li>
    @endforeach
</ul>
