<h2>{{ $block->document()->getTitle() }}</h2>
{!! $block->document()->getContent() !!}
<a href="{{ route('documents.edit', compact('document')) }}">{{ __('Edit') }}</a>